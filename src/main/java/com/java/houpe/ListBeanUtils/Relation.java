package com.java.houpe.ListBeanUtils;

import java.lang.annotation.*;

/**
 * @author Blues
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
public @interface Relation {
    /**
     * 服务名
     *
     * @return
     */
    Class<?> className();

    /**
     * @return
     */
    String method();

    /**
     * 外键id
     *
     * @return
     */
    String targetKey();

    /**
     * 内键ID
     *
     * @return
     */
    String sourceKey();

    /**
     * 是不是bean. 如果不是就直接new
     * @return
     */
    boolean bean() default true;
}
