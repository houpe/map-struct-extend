package com.java.houpe.ListBeanUtils;

import lombok.Builder;
import lombok.Data;

/**
 * @author Blues
 */
 @Data
 @Builder
public class SourceServicesObj{
    /**
     * 服务名
     *
     * @return
     */
   private Class<?> className;

    /**
     * @return
     */
    private String method;

    /**
     * 外键id
     *
     * @return
     */
    private  String queryField;

    /**
     * 内键ID
     *
     * @return
     */
    private  String sourceKey;

    /**
     * 服务名
     *
     * @return
     */
    private Class<?> fieldValue;
}
