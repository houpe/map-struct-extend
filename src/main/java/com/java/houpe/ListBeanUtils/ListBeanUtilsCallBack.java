package com.java.houpe.ListBeanUtils;

@FunctionalInterface
public interface ListBeanUtilsCallBack<S, T> {
    void callBack(S t, T s);
}
