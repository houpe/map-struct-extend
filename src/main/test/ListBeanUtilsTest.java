import com.java.houpe.ListBeanUtils.*;
import com.java.houpe.vo.Vo;
import com.java.houpe.vo.servicesInstance;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringRunner;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


/**
 * @author Blues
 * @date 2021/10/12 2:09 下午
 **/
@Slf4j
@RunWith(SpringRunner.class)
public class ListBeanUtilsTest {
    Logger logger = LoggerFactory.getLogger(ListBeanUtilsTest.class);

    @Test
    public void main() {
//        BiFunction<SysDictClient, String, Result<DictVO>> method = SysDictClient::getBaseByCode;
        List<Entity> entities = createEntity(10);
        List<EntityRel> entityRel = createEntityRel(10);

        //最简单的使用
        List<Vo> result = ListBeanUtils.copyListProperties(entities, Vo::new);

        //提取id集合,方便二次数据库查查
        List<Class<Long>> ids = ListBeanUtils.getArrayListKeys(entities, "belongId", Long.class);
        //entityRel mapper.findByIds(ids);
        //先把v
        List<Vo2> vo2s = ListBeanUtils.copyListProperties(entityRel, Vo2::new);
        //回调使用
        result = ListBeanUtils.copyListProperties(entities, Vo::new, (entity, vo) -> {
            Vo2 vo2 = ListBeanUtils.findObject(vo2s, entity.getBelongId(), "id");
//            vo.setRel(vo2);
        });
        System.out.println(result);

    }

    @Test
    // 传入规则的方式声明. 不需要任何注解
    public void advUse1() {
        List<Entity> entities = createEntity(10);
        List<EntityRel> entityRel = createEntityRel(10);
        List<RuleMap> rules = Arrays.asList(
                new RuleMap("rel", "id", entityRel),
                new RuleMap("name", "id" )
        );
        //这样就行了
        //List<Vo> result = ListBeanUtils.copyListProperties(entities,Vo::new,rules);
        List<Vo> result = ListBeanUtils.copyListProperties(entities, Vo::new,rules,(entity, vo) -> {
            //如果有啥其他想处理的 也可以写这里
        });
        System.out.println(result);
    }

    @Test
    public void advUse22() throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        Entity t = new Entity();
        t.setId(9999);
//        t.setName(getRandomString(10));
        t.setBelongId(1024L);

        Vo vo = new Vo();

        ListBeanUtils.copyPropertiesWithRelation(t, vo,null,null);
        System.out.printf(vo.toString());
    }


    @Test
    public void invokeTest() throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {

        Class tClass = Class.forName("com.java.houpe.vo.servicesInstance");


        Method method = ReflectUtils.getMethodByName(tClass, "getRelObj");


        Object obj2= method.invoke((Object)tClass.newInstance(),new Integer(999));


        System.out.printf(obj2.toString());


    }
    @Test
    //利用注解的方式快速声明, 见Relation
    public void advUse2() {
//        List<Entity> entities = createEntity(10);
//        //自动扫描注解, 会先提取所有id,再读取服务, 再匹配
//        List<Vo> result = ListBeanUtils.copyListPropertiesWithRelation(entities, Vo::new);
//        System.out.println(result);
    }






    List<Entity> createEntity(int number) {
        List<Entity> testDtos = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            Entity t = new Entity();
            t.setId(i);
            t.setName(getRandomString(10));
            t.setBelongId(i);
            testDtos.add(t);
        }
        return testDtos;
    }


    List<EntityRel> createEntityRel(int number) {
        List<EntityRel> vo = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            EntityRel t = new EntityRel();
            t.setId(i);
            t.setName("rel" + getRandomString(10));
            t.setPassword("password:" + getRandomString(10));
            vo.add(t);
        }
        return vo;
    }

    @Data
    class Entity {
        private long id;
        private long belongId;
        private String name;
    }

    @Data
    class EntityRel {
        private long id;
        private String name;
        private String password;
    }



    @Data
    class Vo2 {
        private long id;
        private String name;
    }

    class vo2Service {
        List<EntityRel> getByIds(List<Long> ids){
            return createEntityRel(10);
        }
    }


    private String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }
}
