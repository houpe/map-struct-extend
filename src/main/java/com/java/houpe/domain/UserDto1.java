package com.java.houpe.domain;

import lombok.*;

import java.time.LocalDateTime;

/**
 * @author ：liuhongjiang
 * @description：TODO
 * @date ：2020/3/31 0031 17:04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class UserDto1 {
    private Integer id;
    private String name;
    private String createTime;
    private LocalDateTime updateTime;
}
