package com.java.houpe.mapper;


import com.java.houpe.domain.UserDto1;
import com.java.houpe.domain.UserDto3;
import com.java.houpe.vo.UserVo1IdNamedUserId;
import com.java.houpe.vo.Vo3and4;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * @Classname UserMapper
 * @Description TODO
 * @Date 2021/10/13 2:28 下午
 * @Created by houpeng
 */
@Mapper(componentModel = "spring")
public interface UserConver {

    UserConver INSTANCE = Mappers.getMapper(UserConver.class);



    Vo3and4 Vo3and4Convert(UserDto3 source);

    /**
     * 字段类型一致，名字不一致
     * @param source
     * @return
     */
    @Mappings({
            @Mapping(source = "id", target = "userId"),

    })
    UserVo1IdNamedUserId toUserV01IdNamedUserId(UserDto1 source);
    /**
     * 字段类型一致，名字不一致
     * @param source
     * @return
     */


}

