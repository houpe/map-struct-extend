package com.java.houpe.ListBeanUtils;

import java.lang.annotation.*;

/**
 * @author Blues
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
public @interface SourceKey {

    String value();

}
