package com.java.houpe.vo;

import lombok.*;

import java.time.LocalDateTime;

/**
 * @author ：liuhongjiang
 * @description：TODO
 * @date ：2020/3/31 0031 17:04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class UserVo1IdNamedUserId {
    private Integer userId;
    private String name;
    private String createTime;
    private LocalDateTime updateTime;
}
