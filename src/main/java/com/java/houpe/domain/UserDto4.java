package com.java.houpe.domain;

import lombok.*;

/**
 * @author ：liuhongjiang
 * @description：TODO
 * @date ：2020/3/31 0031 17:04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class UserDto4 {

    private int id;
    private int relationId;
    private String name;



}
