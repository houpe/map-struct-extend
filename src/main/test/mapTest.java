
import com.java.houpe.domain.UserDto1;
import com.java.houpe.domain.UserDto2;
import com.java.houpe.domain.UserDto3;
import com.java.houpe.domain.UserDto4;

import com.java.houpe.mapper.UserConver;
import com.java.houpe.vo.Vo3and4;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

/**
 * @Classname mapTest
 * @Description TODO
 * @Date 2021/10/13 2:15 下午
 * @Created by houpeng
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = mapTest.class)
public class mapTest {
    UserDto1 userDto1;
    UserDto2 userDto2;
    ArrayList<Vo3and4> vo3and4ArrayList = new ArrayList<Vo3and4>();
    ArrayList<UserDto3> userDto3ArrayList = new ArrayList<UserDto3>();
    ArrayList<UserDto4> userDto4ArrayList = new ArrayList<UserDto4>();

    void initData() {
        //1.模拟一个dtos

        for (int i = 0; i < 10; i++) {
            userDto3ArrayList.add(UserDto3.builder().id(i).name("name"+i)
                    .build());
        }


        for (int i = 0; i < 10; i++) {
            userDto4ArrayList.add(UserDto4.builder().id(i).relationId(i)
                    .build());
        }
    }


    //2.单个，属性对应的dto 转 vo
    @Test
    public void T0() {
        initData();
        userDto3ArrayList.forEach((e)->{
            Vo3and4 t=   UserConver.INSTANCE.Vo3and4Convert(e);
            userDto4ArrayList.forEach((f)->{
                 if (t.getId()==f.getRelationId())
                 {
                     t.setDto4(f);
                 }
            });
            vo3and4ArrayList.add(t);

        });

        vo3and4ArrayList.forEach((e)->{
            System.out.println(e.toString());
        });
    }

    //3.单个，属性不对应的dto 转 vo
    @Test
    public void T2() {
        Supplier<String> supplier = String::new;
        System.out.println(supplier.get());//
    }





    List<UserDto1> createEntity(int number) {
        List<UserDto1> testDtos = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            UserDto1 t = new UserDto1();
            t.setId(i);
            t.setName(getRandomString(10));
            testDtos.add(t);
        }
        return testDtos;
    }


    List<UserDto2> createEntityRel(int number) {
        List<UserDto2> vo = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            UserDto2 t = new UserDto2();
            t.setId(i);
            t.setName("rel" + getRandomString(10));
            vo.add(t);
        }
        return vo;
    }


    private String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }
}
