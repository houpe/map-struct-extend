package com.java.houpe.vo;

import com.java.houpe.ListBeanUtils.SourceKey;
import com.java.houpe.ListBeanUtils.SourceServices;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname Vo
 * @Description TODO
 * @Date 2021/10/14 3:36 下午
 * @Created by houpeng
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Vo {

//        @SourceKey("id")
//        private long ids;
//
//        @SourceKey("name")
//
//        private String MZ;

    @SourceKey("id")
    public long belongId;

    @SourceServices(className = servicesInstance.class, method = "getRelObj", queryField = "belongId")
    public UserVo1 rel;
}
