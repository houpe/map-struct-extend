package com.java.houpe.ListBeanUtils;

import lombok.Data;

/**
 * @author Blues
 * @date 2021/10/12 2:56 下午
 **/
@Data
public class ListBeanUtilsRule<T> {
    private T sourceKey;
    private T targetKey;


}
