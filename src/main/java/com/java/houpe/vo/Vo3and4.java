package com.java.houpe.vo;

import com.java.houpe.domain.UserDto4;
import lombok.*;

/**
 * @author ：liuhongjiang
 * @description：TODO
 * @date ：2020/3/31 0031 17:04
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Setter
@Getter
public class Vo3and4 {
    private int id;
    private String name;
    private UserDto4 dto4;
}
